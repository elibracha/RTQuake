package src.controllers;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import src.ingest.data.DataIngestion;

public class DataServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		String start_date = request.getParameter("st");
		String end_date = request.getParameter("ed");
		
		response.setContentType("application/json");
		PrintWriter out = response.getWriter();
		
		DataIngestion ingestor = null;
		try {

			if ( start_date != null && end_date != null) {
				ingestor = new DataIngestion(start_date, end_date);
			} else {
				ingestor = new DataIngestion();
			}

			out.println(ingestor.parallel_ingetion());

		} catch (Exception e) {
			System.out.println("Exception in NetClientGet:- " + e);
		} 

	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		this.doGet(request, response);
	}

}
