package src.ingest.data;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import com.google.gson.Gson;

import src.model.pointofInterest.PointOfInterest;
import src.model.pointofInterest.PointOfInterestFactorey;
import src.model.response.USGSResponse;

public class DataIngestion {

	private static final String url = "https://earthquake.usgs.gov/fdsnws/event/1/query?format=geojson";
	private String start_ingest_date = null;
	private String end__ingest_date = null;
	
	public DataIngestion(){
		this.start_ingest_date = this.date_to_string(yesterday());
		this.end__ingest_date = this.date_to_string(new Date());
	}
	
	public DataIngestion(String start_date, String end_Date) {
		this.start_ingest_date = this.date_to_string(this.string_to_date(start_date));
		this.end__ingest_date = this.date_to_string(this.string_to_date(end_Date));
	}
	
	private Date string_to_date(String date) {
		DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		try {
			return format.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	private String date_to_string(Date date) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		return dateFormat.format(date);
	}
	
	private Date yesterday() {
		final Calendar cal = Calendar.getInstance();
	    cal.add(Calendar.DATE, -1);
	    return cal.getTime();
	}
	
	public String parallel_ingetion() {
		ExecutorService executor = Executors.newFixedThreadPool(1);

		Future<String> future = executor.submit(() -> {
		        return ingest_data();
		});

		try {
			return future.get(60, TimeUnit.SECONDS);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} 
	}
	
	private String ingest_data(){
		 
		 ArrayList<PointOfInterest> points = null;
		 Gson gson = new Gson();
		 
		try {
			URL url = new URL(DataIngestion.url + append_time_fillter());
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();

			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");

			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP Error code : " + conn.getResponseCode());

			}

			InputStreamReader in = new InputStreamReader(conn.getInputStream());
			BufferedReader br = new BufferedReader(in);

			points = PointOfInterestFactorey.get_instance().build_point_from_response(gson.fromJson(br, USGSResponse.class));
			
			conn.disconnect();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return gson.toJson(points);
	}
	
	private String append_time_fillter() {
		return "&starttime=".concat(this.start_ingest_date).concat("&endtime=").concat(this.end__ingest_date);
	}
	
}
