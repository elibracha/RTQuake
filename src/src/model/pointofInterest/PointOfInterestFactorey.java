package src.model.pointofInterest;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import src.model.response.USGSResponse;
import src.model.response.USGSResponseFeatures;

public class PointOfInterestFactorey {
	
	private static PointOfInterestFactorey factorey = new PointOfInterestFactorey();
	
	private PointOfInterestFactorey() {	}
	
	public static PointOfInterestFactorey get_instance() {
		return factorey;
	}
	
	public ArrayList<PointOfInterest> build_point_from_response(USGSResponse response) {
		
		ArrayList<PointOfInterest> points = new ArrayList<>();
		
		for( USGSResponseFeatures feature : response.getFeatures() ){
			
			SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
			Date date = new Date(feature.getProperties().getTime());
			String time = sf.format(date);
			
			points.add(new PointOfInterest(feature.getProperties().getMag(), 
										   feature.getProperties().getTitle(),
										   feature.getGeometry().getCoordinates(),
										   time));
			
		}
		
		return points;
		
	}

}
