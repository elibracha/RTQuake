package src.model.pointofInterest;

import java.util.Arrays;

public class PointOfInterest {
	
	private double mag;
	private String title;
	private double[] coordinates;
	private String date;
	
	public PointOfInterest(double mag, String title, double[] coordinates, String date) {
		this.mag = mag;
		this.title = new String(title);
		this.coordinates = coordinates;
		this.date = date;
	}
	
	public double getMag() {
		return mag;
	}
	public void setMag(double mag) {
		this.mag = mag;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public double[] getCoordinates() {
		return coordinates;
	}
	public void setCoordinates(double[] coordinates) {
		this.coordinates = coordinates;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "PointOfInterest [mag=" + mag + ", title=" + title + ", coordinates=" + Arrays.toString(coordinates)
				+ ", date=" + date + "]";
	}
	
	
}
