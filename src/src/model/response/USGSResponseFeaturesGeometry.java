package src.model.response;

import java.util.Arrays;

public class USGSResponseFeaturesGeometry {
	
	private String type;
	private double[] coordinates;
	
	public String getType() {
		return type;
	}
	public double[] getCoordinates() {
		return coordinates;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	public void setCoordinates(double[] coordinates) {
		this.coordinates = coordinates;
	}
	
	@Override
	public String toString() {
		return "USGSResponseFeaturesGeometry [type=" + type + ", coordinates=" + Arrays.toString(coordinates) + "]";
	}
	
	
}
