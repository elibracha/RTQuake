package src.model.response;

public class USGSResponseMetaData {

	private String generated;
	private String url;
	private String title;
	private String status;
	private String api;
	private String count;
	
	public String getGenerated() {
		return generated;
	}
	public void setGenerated(String generated) {
		this.generated = generated;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getApi() {
		return api;
	}
	public void setApi(String api) {
		this.api = api;
	}
	public String getCount() {
		return count;
	}
	public void setCount(String count) {
		this.count = count;
	}
	
	@Override
	public String toString() {
		return "USGSResponseMetaData [generated=" + generated + ", url=" + url + ", title=" + title + ", status="
				+ status + ", api=" + api + ", count=" + count + "]";
	}
	
}
