package src.model.response;

import java.util.Arrays;

public class USGSResponse {

	private String type;
	private USGSResponseMetaData metadata;
	private USGSResponseFeatures[] features;
	private double[] bbox;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public USGSResponseMetaData getMetadata() {
		return metadata;
	}
	public void setMetadata(USGSResponseMetaData metadata) {
		this.metadata = metadata;
	}
	public USGSResponseFeatures[] getFeatures() {
		return features;
	}
	public void setFeatures(USGSResponseFeatures[] features) {
		this.features = features;
	}
	public double[] getBbox() {
		return bbox;
	}
	public void setBbox(double[] bbox) {
		this.bbox = bbox;
	}
	
	@Override
	public String toString() {
		return "USGSResponse [type=" + type + ",\n metadata=" + metadata + ",\n feattures=" + Arrays.toString(features)
				+ ",\n bbox=" + Arrays.toString(bbox) + "]";
	}
	
}
