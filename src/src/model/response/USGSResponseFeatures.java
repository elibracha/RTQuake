package src.model.response;

public class USGSResponseFeatures {

	private String type;
	private USGSResponseFeaturesProperties properties;
	private USGSResponseFeaturesGeometry geometry;
	private String id;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public USGSResponseFeaturesProperties getProperties() {
		return properties;
	}
	public void setProperties(USGSResponseFeaturesProperties properties) {
		this.properties = properties;
	}
	public USGSResponseFeaturesGeometry getGeometry() {
		return geometry;
	}
	public void setGeometry(USGSResponseFeaturesGeometry geometry) {
		this.geometry = geometry;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	 
	@Override
	public String toString() {
		return "USGSResponseFeatures [type=" + type + ", properties=" + properties + ", geometry=" + geometry + ", id="
				+ id + "]";
	}
	
	
}
