<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<title>RTQuake Map</title>
<meta name="viewport" content="initial-scale=1.0">
<meta charset="utf-8">
<script type="text/javascript">

	var infoWindow = null;
	var start_date = null;
	var end_date = null;
	var markers = [];
	
	function load_data() {
		document.getElementById("loader").style.visibility = "visible";
		
		var xhttp = new XMLHttpRequest();

		xhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				apply_points(JSON.parse(this.responseText));
				document.getElementById("loader").style.visibility = "hidden";
			}
		};
		
		if (start_date && end_date){
			xhttp.open("GET", "http://localhost:8080/RTQuake/DataServlet?st=" + start_date + "&ed=" + end_date, true);
		} else{
			xhttp.open("GET", "http://localhost:8080/RTQuake/DataServlet", true);
		}
		xhttp.send();
	}
	
	function update_dates() {
		start_date = document.getElementById("st").value
		end_date = document.getElementById("ed").value
		load_data();
	}

	function apply_points(points) {
		setMapOnAll(null);
		markers = [];
		points.forEach(function(element) {
			create_marker(element);
		});
		setMapOnAll(map)
	}

	function create_marker(point) {
		var location = {
			lat : point.coordinates[1],
			lng : point.coordinates[0]
		};

		var contentString = '<div id="content">' + '<h4> Location : '
				+ point.title + '</h4>' + '<h4> Mag : ' + point.mag + '<h4>'
				+ '<h4> Time : ' + point.date + '<h4>' + '</div>';

		var color = 'yellow';

		if (point.mag >= 3.5) {
			color = 'orange';
		}
		if (point.mag >= 6.5) {
			color = 'red';
		}

		var marker = new google.maps.Marker({
			position : location,
			map : map,
			icon : {
				path : google.maps.SymbolPath.CIRCLE,
				fillColor : color,
				fillOpacity : .6,
				scale : Math.pow(2, point.mag) / 2.5,
				strokeColor : 'white',
				strokeWeight : .5
			}
		});

		google.maps.event.addListener(marker, 'click', function() {

			if (infoWindow != null) {
				infoWindow.close()
			}

			infoWindow = new google.maps.InfoWindow({
				content : contentString
			});

			infoWindow.open(map, marker);
		});

		markers.push(marker);

	}

	function setMapOnAll(map) {
		for (var i = 0; i < markers.length; i++) {
			markers[i].setMap(map);
		}
	}

	/* Set the width of the side navigation to 250px and the left margin of the page content to 250px and add a black background color to body */
	function openNav() {
		document.getElementById("mySidenav").style.width = "200px";
		document.getElementById("map").style.marginLeft = "200px";
		document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
	}

	/* Set the width of the side navigation to 0 and the left margin of the page content to 0, and the background color of body to white */
	function closeNav() {
		document.getElementById("mySidenav").style.width = "0";
		document.getElementById("map").style.marginLeft = "0";
		document.body.style.backgroundColor = "white";
	}

	setInterval(function() {
		load_data();
	}, 60000 * 5);
</script>
<style>
#map {
	height: 100%;
}

html, body {
	height: 100%;
	margin: 0;
	padding: 0;
	position: relative;
}

/* The side navigation menu */
.sidenav {
	height: 100%; /* 100% Full-height */
	width: 0; /* 0 width - change this with JavaScript */
	position: fixed; /* Stay in place */
	z-index: 1; /* Stay on top */
	top: 0; /* Stay at the top */
	left: 0;
	background-color: #ededed; /* Black*/
	overflow-x: hidden; /* Disable horizontal scroll */
	padding-top: 60px; /* Place content 60px from the top */
	transition: 0.5s;
	/* 0.5 second transition effect to slide in the sidenav */
}

/* The navigation menu links */
.sidenav a, .sidenav div {
	padding: 8px 8px 8px 32px;
	text-decoration: none;
	font-size: 25px;
	color: #818181;
	display: block;
	transition: 0.3s;
}

/* When you mouse over the navigation links, change their color */
.sidenav a:hover {
	color: #111;
}

/* Position and style the close button (top right corner) */
.sidenav .closebtn {
	position: absolute;
	top: 0;
	right: 25px;
	font-size: 36px;
	margin-left: 50px;
}

/* Style page content - use this if you want to push the page content to the right when you open the side navigation */
#main {
	transition: margin-left .5s;
	padding: 20px;
}
#mySidenav #loader {
  border: 16px solid #f3f3f3; /* Light grey */
  border-top: 16px solid #3498db; /* Blue */
  border-radius: 50%;
  width: 120px;
  height: 120px;
  animation: spin 2s linear infinite;
  padding: 0;
  position: relative;
  z-index: 99999; 
  margin: auto; 
 visibility: hidden;
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}

/* On smaller screens, where height is less than 450px, change the style of the sidenav (less padding and a smaller font size) */
@media screen and (max-height: 450px) {
	.sidenav {
		padding-top: 15px;
	}
	.sidenav a {
		font-size: 18px;
	}
}
</style>
</head>
<body>
	<div id="mySidenav" class="sidenav">
		<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
		<div>
			Start Date: <input id="st" type="date" name="start_date"> End Date: <input
				id="ed" type="date" name="end_date"> <button type="submit" style="margin-bottom: 50%;" onclick="update_dates()">submit</button>
		</div>
		<div id="loader"></div>
	</div>

	<!-- Use any element to open the sidenav -->
	<span onmouseover="openNav()" style="bottom: 0; position: fixed; width:200px; height: 100px; z-index: 999999;"></span>
	<div id="map"></div>
	<script>
		var map;
		function initMap() {
			map = new google.maps.Map(document.getElementById('map'), {
				zoom : 2,
				center : new google.maps.LatLng(2.8, -187.3),
				mapTypeId : 'terrain'
			});
			load_data();
		}
	</script>
	<script
		src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBCxXbsoKQwe5wwwZpoBZDcAC8X9zKxHC0&callback=initMap"
		async defer></script>
</body>
</html>